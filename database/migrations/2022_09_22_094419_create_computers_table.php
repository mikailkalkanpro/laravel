<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('computers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->string('stock_type');
            $table->tinyInteger('is_available');
            $table->string('image');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
        
        Schema::table('lends', function (Blueprint $table) {
            $table->unsignedBigInteger('computer_id')->index()->after('user_id');
            $table->foreign('lend_id')->references('id')->on('lends')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('computers');
    }
};
