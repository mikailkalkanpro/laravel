<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repairs', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->tinyInteger('is_repaired');
            $table->tinyInteger('is_broken');
            $table->datetime('returned_at');
            $table->datetime('created_at');
            $table->datetime('updated_at');
        });
        Schema::table('repairers', function (Blueprint $table) {
            $table->unsignedBigInteger('repairer_id')->index()->after('password');
            $table->foreign('repairer_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repairs');
    }
};
