<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRepairs_typesRequest;
use App\Http\Requests\UpdateRepairs_typesRequest;
use App\Models\Repairs_types;

class RepairsTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRepairs_typesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRepairs_typesRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Repairs_types  $repairs_types
     * @return \Illuminate\Http\Response
     */
    public function show(Repairs_types $repairs_types)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Repairs_types  $repairs_types
     * @return \Illuminate\Http\Response
     */
    public function edit(Repairs_types $repairs_types)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRepairs_typesRequest  $request
     * @param  \App\Models\Repairs_types  $repairs_types
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRepairs_typesRequest $request, Repairs_types $repairs_types)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Repairs_types  $repairs_types
     * @return \Illuminate\Http\Response
     */
    public function destroy(Repairs_types $repairs_types)
    {
        //
    }
}
