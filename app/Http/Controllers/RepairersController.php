<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRepairersRequest;
use App\Http\Requests\UpdateRepairersRequest;
use App\Models\Repairers;

class RepairersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRepairersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRepairersRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Repairers  $repairers
     * @return \Illuminate\Http\Response
     */
    public function show(Repairers $repairers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Repairers  $repairers
     * @return \Illuminate\Http\Response
     */
    public function edit(Repairers $repairers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRepairersRequest  $request
     * @param  \App\Models\Repairers  $repairers
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRepairersRequest $request, Repairers $repairers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Repairers  $repairers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Repairers $repairers)
    {
        //
    }
}
