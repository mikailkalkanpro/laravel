<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreComputersRequest;
use App\Http\Requests\UpdateComputersRequest;
use App\Models\Computers;

class ComputersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreComputersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComputersRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Computers  $computers
     * @return \Illuminate\Http\Response
     */
    public function show(Computers $computers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Computers  $computers
     * @return \Illuminate\Http\Response
     */
    public function edit(Computers $computers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateComputersRequest  $request
     * @param  \App\Models\Computers  $computers
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateComputersRequest $request, Computers $computers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Computers  $computers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Computers $computers)
    {
        //
    }
}
