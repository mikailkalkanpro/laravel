<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLendsRequest;
use App\Http\Requests\UpdateLendsRequest;
use App\Models\Lends;

class LendsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreLendsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLendsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lends  $lends
     * @return \Illuminate\Http\Response
     */
    public function show(Lends $lends)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lends  $lends
     * @return \Illuminate\Http\Response
     */
    public function edit(Lends $lends)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateLendsRequest  $request
     * @param  \App\Models\Lends  $lends
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLendsRequest $request, Lends $lends)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lends  $lends
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lends $lends)
    {
        //
    }
}
