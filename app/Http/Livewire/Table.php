<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Table extends Component
{
    public string $message ="Hello World";


    public function render()
    {
        return view('livewire.table');
    }

    public function changeMessage()
    {
        $this->dispatchBrowserEvent('oneEvent',[
            'message' => "TOTO"
        ]);
    }
}
