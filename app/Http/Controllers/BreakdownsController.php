<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBreakdownsRequest;
use App\Http\Requests\UpdateBreakdownsRequest;
use App\Models\Breakdowns;

class BreakdownsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBreakdownsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBreakdownsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Breakdowns  $breakdowns
     * @return \Illuminate\Http\Response
     */
    public function show(Breakdowns $breakdowns)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Breakdowns  $breakdowns
     * @return \Illuminate\Http\Response
     */
    public function edit(Breakdowns $breakdowns)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBreakdownsRequest  $request
     * @param  \App\Models\Breakdowns  $breakdowns
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBreakdownsRequest $request, Breakdowns $breakdowns)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Breakdowns  $breakdowns
     * @return \Illuminate\Http\Response
     */
    public function destroy(Breakdowns $breakdowns)
    {
        //
    }
}
